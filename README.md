# SASS framework

Its CSS Framework that will help you thin out unnused code by granting you controll over your assets.

## Installation

No 3rd party dependencies are required at the moment. Just plug and play.

## Usage

```TBS```

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## Credits

See contributors.txt

## License

You are free to fork, edit and use.