'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('build-dev', function (done) {

	var sassOptions = {
		errLogToConsole: true,
		outputStyle: 'expanded'
	}, minifyOptions = {
		keepSpecialComments: 1
	};
	
	gulp.src('./src/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass(sassOptions).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dist'))
		.on('end', done);
});

gulp.task('build-prod', function (done) {

	var sassOptions = {
		errLogToConsole: true,
		outputStyle: 'expanded'
	}, minifyOptions = {
		keepSpecialComments: 0
	};
	
	gulp.src('./src/**/*.scss')
		.pipe(sass(sassOptions).on('error', sass.logError))
		.pipe(minifyCss(minifyOptions))
		.pipe(rename({ extname: '.min.css' }))
		.pipe(gulp.dest('./dist'))
		.on('end', done);
});
		

gulp.task('watch', function () {
	return gulp
		.watch('./src/*.scss', ['build-dev'])
		.on('change', function (event) {
			console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
		});
});

gulp.task('dev', ['build-dev', 'watch']);